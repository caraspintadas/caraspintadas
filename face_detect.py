#!/usr/bin/python

# face_detect.py

# Face Detection using OpenCV. Based on sample code from:
# http://python.pastebin.com/m76db1d6b

# Usage: python face_detect.py <image_file>

import sys, os
from opencv.cv import *
from opencv.highgui import *

def detectObjects(image):
  """Converts an image to grayscale and prints the locations of any 
     faces found"""
  grayscale = cvCreateImage(cvSize(image.width, image.height), 8, 1)
  cvCvtColor(image, grayscale, CV_BGR2GRAY)

  storage = cvCreateMemStorage(0)
  cvClearMemStorage(storage)
  cvEqualizeHist(grayscale, grayscale)
  cascade = cvLoadHaarClassifierCascade(
    'haarcascade_frontalface_default.xml',
    cvSize(1,1))
  faces = cvHaarDetectObjects(grayscale, cascade, storage, 1.2, 2,
                             CV_HAAR_DO_CANNY_PRUNING, cvSize(50,50))

  if faces.total > 0:
    for f in faces:
      print("[(%d,%d) -> (%d,%d)]" % (f.x, f.y, f.x+f.width, f.y+f.height))
      paintfaces(sys.argv[1], f)

def paintfaces(filename, f):
    os.system("convert %s -fill green -colorize 65 green-%s" % (filename, filename))
    os.system("convert %s -fill yellow -colorize 65 yellow-%s" % (filename, filename))
    os.system("convert %s -fill black -colorize 100 -fill white -draw 'rectangle %d,%d %d,%d' leftmask-%s" % (filename, f.x,f.y, f.x+(f.width/2),f.y+f.height, filename))
    os.system("convert %s -fill black -colorize 100 -fill white -draw 'rectangle %d,%d %d,%d' rightmask-%s" % (filename, f.x+(f.width/2),f.y, f.x+f.width,f.y+f.height, filename))
    os.system("convert %s green-%s leftmask-%s -composite %s" % (filename, filename, filename, filename))
    os.system("convert %s yellow-%s rightmask-%s -composite %s" % (filename, filename, filename, filename))

def main():
  image = cvLoadImage(sys.argv[1]);
  detectObjects(image)

if __name__ == "__main__":
  main()

